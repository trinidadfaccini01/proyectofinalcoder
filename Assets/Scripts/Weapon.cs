using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public enum TypeOfWeapon{
        TYPE1,        
        TYPE2,
        TYPE3,
        TYPE4
    }

    public GameObject bullet;
    public GameObject[] spawnPoints;
    private float destroyTime = 10;
    protected float time;
    protected float timeRemaining;
    public Transform objToFollow;
    public float launchVelocity;
    protected float speed;
    public TypeOfWeapon typeOfWeapon;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dist = CalculateDistance();
        BulletTimer(dist);

        // ARMAS EN ALTURA
        if(typeOfWeapon == TypeOfWeapon.TYPE1 && dist.z < 0 || typeOfWeapon == TypeOfWeapon.TYPE2 && dist.z > 0){
            LookAtPlayerLerp();
        }

        // ARMAS A NIVEL CALLE
        if(typeOfWeapon == TypeOfWeapon.TYPE3 && dist.z < 0 || typeOfWeapon == TypeOfWeapon.TYPE4 && dist.z > 0){
            LookAtPlayerLerp();
        }
    }

    private Vector3 CalculateDistance() {

        Vector3 dist = objToFollow.position - transform.position;
        // Debug.Log("vector" + dist);
        // Debug.Log("magnitud" + dist.magnitude);
        return dist;
    }

    void Shoot() 
    {
        GameObject bulletOnScreen;
        for(int i = 0; i < spawnPoints.Length; i++){
            bulletOnScreen =Instantiate(bullet,spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position, transform.rotation);
            bulletOnScreen.GetComponent<Rigidbody>().AddRelativeForce(new Vector3(0,0,launchVelocity));
            Destroy(bulletOnScreen, destroyTime);
        }
    }

    void LookAtPlayerQuaternion() {
        // Creamos un vector que va a devolver la posicion del jugador a la que yo quiero ver
        Quaternion rot = Quaternion.LookRotation(objToFollow.position - transform.position);
        transform.rotation = rot;
    }


    void LookAtPlayerLerp() {
       Quaternion lookRotation = Quaternion.LookRotation(((objToFollow.position - transform.position).normalized));
       transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation  , Time.deltaTime * speed);
    }


    void LookAtPlayer() {
        transform.LookAt(objToFollow);
    }

    void BulletTimer(Vector3 dist) {

        if(timeRemaining > 0){
            timeRemaining -= Time.deltaTime;
        }

        else { 
            if((typeOfWeapon == TypeOfWeapon.TYPE1 || typeOfWeapon == TypeOfWeapon.TYPE2) && dist.magnitude < 100){
                if(typeOfWeapon == TypeOfWeapon.TYPE1 && dist.z < 0 || typeOfWeapon == TypeOfWeapon.TYPE2 && dist.z > 0){
                    Debug.Log("aca3");
                    Shoot();
                }
            }


            if(typeOfWeapon == TypeOfWeapon.TYPE3 || typeOfWeapon == TypeOfWeapon.TYPE4){
                RaycastHit hit;
                if(Physics.Raycast(spawnPoints[0].transform.position, spawnPoints[0].transform.forward, out hit, 70f)){
                    Debug.Log(dist.magnitude);
                    if(hit.transform.gameObject.CompareTag("Player")) Shoot();
                }
            }
            timeRemaining = time;
        }
        
    }
}
