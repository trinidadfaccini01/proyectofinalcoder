using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    [Header ("Construccion")]
    public GameObject shieldPrefab;
    public GameObject lifePrefab;
    public GameObject coinPrefab;
    
    public Transform[] shieldPoints;
    void Start()
    {
        // for(int i = 0; i < 3; i++){
        //     Instantiate(prefab1, new Vector3(i,0,5), transform.rotation);
        // }

        for(int i = 0; i < shieldPoints.Length; i++){
            Instantiate(shieldPrefab,shieldPoints[Random.Range(0, shieldPoints.Length)].position, transform.rotation);
            // aca deberiamos chequear si el punto ya fue utilizado para que no se posicionen en el mismo lugar dos veces 
        }
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
