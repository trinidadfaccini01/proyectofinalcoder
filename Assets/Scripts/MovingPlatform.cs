using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public GameObject[] movingPoints;
    int currentPointIndex = 0;

    public float speed = 10f;

    // Start is called before the first frame update
    void Start()
    {
        currentPointIndex = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(transform.position,movingPoints[currentPointIndex].transform.position) < 0.1f){
            togglePoint();
        }
        transform.position = Vector3.MoveTowards(transform.position, movingPoints[currentPointIndex].transform.position,speed * Time.deltaTime);

    }

    void togglePoint()
    {
        if(currentPointIndex == 0)
            currentPointIndex = 1;
        else currentPointIndex = 0;
    }
}
