using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameEvents : MonoBehaviour
{
    public AudioSource _audSource;

    [Header ("Pausa")]
    public GameObject pauseMenu;
    public GameObject hud;

    public void AppearImage(Image img)
    {
        img.color = new Color(255,255,255,255);
    }

    public void DisappearImage(Image img)
    {
        img.enabled = false;
        //img.color = new Color(255,255,255,0);
    }

    public void DisappearText(Text txt)
    {
        txt.enabled = false;
    }

    public void StopTime()
    {
        Time.timeScale = 0;
    }

    public void ResumeTime()
    {
        Time.timeScale = 1;
    }

    public void PlaySound(AudioClip clip)
    {
        _audSource.clip = clip;
        _audSource.Play();
    }
    
    public void Respawn(MovableObject obj)
    {
        Rigidbody rb = obj.GetComponent<Rigidbody>();
        rb.velocity = new Vector3(0,0,0);
        obj.transform.position = obj.initialPos;

    }

    public void InstantiateShields(GameObject positions) {
        Instantiate(positions);
    }

    public void ResumeGame()
    {
        Singleton.activePause = false;
        hud.SetActive(true);
        pauseMenu.SetActive(false);
        ResumeTime();
    }

    public void PauseGame()
    {
        Singleton.activePause = true;
        hud.SetActive(false);
        pauseMenu.SetActive(true);
        StopTime();
    }

    public void TogglePause()
    {
        if(Singleton.activePause)
            ResumeGame();
        
        else PauseGame();
        
    }

    public void PauseGameAudio(){
        Singleton.PauseAudio();
    }

    public void PlayGameAudio(){
        Singleton.PlayAudio();
    }
    public void RestartGame(){
        Singleton.playerLife = 100;
        Singleton.playerPoints = 0;
        Singleton.timeRemaining = 200f;
        Singleton.finishedGame = false;
        Singleton.Play();
        PlayGameAudio();
        ResumeTime();
    }

    public void AddGameTime()
    {
        Singleton.timeRemaining += 5;
    }


    public void Exit() 
    {
        Application.Quit();
    }    

}