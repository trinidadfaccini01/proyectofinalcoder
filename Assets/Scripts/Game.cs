using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    public Player carPlayer;

    [ Header ("Camaras y luces")]
    public GameObject cam1;
    public GameObject cam2;
    public Light DirectionalLigth;
    private float directionalLigthtTime = 10;
    private float directionalLigthTimeRemaining = 10;

    [ Header ("HUD")]
    public Text timeText; 
    public Text pointsText; 
    public Image barraVidaVerde;
    public Image barraVidaRoja;

    [ Header ("Juego finalizado")]
    public Text totalTimeText; 
    public Text totalPointsText; 
    public Text title;
    public Text failedGameText;

    [Header ("Barra de vida")]
    private float actualLife;
    private float maxLife = 100f;


    [ Header ("Post Process")]
    public PostProcessVolume volume;
    private ColorGrading _colorGrading;

    [Header ("Eventos")]
    [SerializeField] private UnityEvent onPlayerDeath;
    [SerializeField] private UnityEvent onPlayPause;


    private void Awake() {
        PhysicMaterial pm = carPlayer.GetComponent<Collider>().material;
        volume.profile.TryGetSettings(out _colorGrading);
    }

    void Update()
    {

        /* Camaras*/
        if(Input.GetKeyDown(KeyCode.LeftControl)) ToggleCamera();
        
        /* Apagar y prender luz direccional */
        //TemporizadorDirectionalLight();

        Singleton.timeRemaining -= Time.deltaTime;

        timeText.text = "TIEMPO RESTANTE: " +  Singleton.timeRemaining.ToString("F2") + "s";

        totalTimeText.text = "TIEMPO RESTANTE: " + Singleton.timeRemaining.ToString("F2") + "s";

        pointsText.text = "PUNTOS: " + Singleton.playerPoints;

        totalPointsText.text = "TOTAL PUNTOS: " + Singleton.playerPoints;

        if(Singleton.timeRemaining < 10f){
            timeText.color = new Color(1, 0,0,1);
        }

        if(Singleton.playerPoints > 10){
            pointsText.color = new Color(0,1,0,1);
        }

        //valor que va de 0 a 100 y la imagen va de 0 a 1
        actualLife = Singleton.playerLife;
        barraVidaVerde.fillAmount = Singleton.playerLife/maxLife;

        if(actualLife <= 0 || Singleton.timeRemaining <= 0 || (Singleton.finishedGame && Singleton.playerPoints < 15)) {
            title.text = "HAS PERDIDO EL JUEGO";
            failedGameText.enabled = true;
            failedGameText.text = "No has alcanzado alguno de los objetivos";
            onPlayerDeath.Invoke();
        }

        else {
            title.text = "HAS GANADO EL JUEGO";
        }

        //if(timeRemaining <= 0) onPlayerDeath.Invoke();

        if(Input.GetKeyDown(KeyCode.Escape)) onPlayPause.Invoke();

        if(carPlayer.protectedPlayer && cam1.activeSelf){
            _colorGrading.colorFilter.value = new Color(0.2541522f, 0.2917707f, 1);
        }

        if(carPlayer.protectedPlayer == false){
            _colorGrading.colorFilter.value = new Color(1,1,1);
        }
    }

    private void ToggleCamera(){
        
        if(cam1.activeInHierarchy){
            cam1.SetActive(false);
            cam2.SetActive(true);
        }
        else {
            cam1.SetActive(true);
            cam2.SetActive(false);
        }
    }

    // NO VA EN UN EVENTO PORQUE ES ALGO QUE PASA CUANDO SE INICIA EL JUEGO
    // NO SE ACTIVA CON UN EVENTO
    void TemporizadorDirectionalLight() {
        if(directionalLigthTimeRemaining > 0){
            directionalLigthTimeRemaining -= Time.deltaTime;
        }
        
        else {
            DirectionalLigth.enabled = !DirectionalLigth.enabled;
            directionalLigthTimeRemaining = directionalLigthtTime;
        }
    }


}
