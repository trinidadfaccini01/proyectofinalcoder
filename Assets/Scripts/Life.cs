using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LifeType{
        TYPE1,        
        TYPE2,
}
public class Life : MonoBehaviour
{
    public float lifeTime = 7;
    public float lifeTimeRemaining = 7;
    public LifeType lifeType;
    private bool movable;

    // Start is called before the first frame update
    void Start()
    {
        switch(lifeType){
            case LifeType.TYPE1:
                movable = true;
                break;

            case LifeType.TYPE2:
                break;

            default:
                Debug.Log("Tipo de pared no asignada");
                break;
        } 
    }

    // Update is called once per frame
    void Update()
    {
        if(movable){
            TemporizadorPosicionLife();
        }
    }

    void TemporizadorPosicionLife() {
        if(lifeTimeRemaining > 0){
            lifeTimeRemaining -= Time.deltaTime;
        }
        
        else {
            transform.position += new Vector3(0,0, Random.Range(0, 30));
            lifeTimeRemaining = lifeTime;
        }
    }

}
