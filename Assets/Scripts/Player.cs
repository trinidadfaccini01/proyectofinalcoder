using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering.PostProcessing;

public class Player : MonoBehaviour
{
    [ Header ("Generales")]
    public Vector3 initialPos;

    //public float speed = 20f;
    [ Header ("RB")]
    public float initialMaxSpeed;
    public float maxSpeed;
    private float speedForce = 20f;
    private float jumpForce = 7f;
    public LayerMask groundLayer;
    private float raycastDistance = 5f;
    private bool isGrounded; 
    private Rigidbody rb;
    PhysicMaterial pm;
    // private bool isJumpPressed = false;

    [ Header("SpeedPlatforms")]
    public float speedTime = 7f;
    public float speedTimeRemaining = 7f;
    public bool changeSpeed = false;

    [ Header ("Monedas - vidas - escudos")]
    public float lifeTime = 0;
    public GameObject shieldPositionsPrefab;
    private float shieldTime = 7f;
    public float shieldTimeRemaining = 7f;
    public bool protectedPlayer = false;
    public GameObject shield;

    // public static int points = 0;

    [ Header ("Efectos de audio")]
    public AudioSource horn;
    public AudioSource turningLight; 

    [ Header ("PartycleSystem")]
    public GameObject coinEffect;
    public GameObject lifeEffect;

    [ Header ("Post Process")]
    public PostProcessVolume volume;
    private Vignette _vignette;
    private ChromaticAberration _chromatic;

    [ Header ("Eventos")]
    [SerializeField] private UnityEvent onWallHit;
    [SerializeField] private UnityEvent onBulletHit;
    [SerializeField] private UnityEvent onFinish;
    [SerializeField] private UnityEvent onCoinHit;
    [SerializeField] private UnityEvent onLifeHit;


    private void Awake() {
        rb = GetComponent<Rigidbody>();
        pm = GetComponent<Collider>().material;
        Singleton.playerLife = 100;
        Singleton.playerPoints = 0;
        maxSpeed = initialMaxSpeed;
    }
    void Start()
    {
        initialPos = transform.position;
        volume.profile.TryGetSettings(out _vignette);
        volume.profile.TryGetSettings(out _chromatic);
    }
    private void Update()
    {
        Debug.Log(Singleton.finishedGame);
        /* Sonidos de bocina y guiño */
        if(Input.GetKey(KeyCode.Q)) horn.Play();
        if(Input.GetKey(KeyCode.E)) turningLight.Play();

        if(Input.GetKeyDown(KeyCode.Space)){
            // isJumpPressed = true;
            PlayerJumpPhy();
        }

        if(transform.position.y < -21){
            Singleton.playerLife -= 10;
            Respawn();
            Instantiate(shieldPositionsPrefab);
        }

        if(changeSpeed) SpeedTimer();

        if(protectedPlayer) ShieldTimer();

        if(rb.velocity.magnitude > maxSpeed){
            rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed);
        }

        //Debug.Log(rb.velocity.magnitude);
    }

    void FixedUpdate()
    {
        PlayerMovementPhy();

        // if (isJumpPressed)
        // {
        //     if(rb.gameObject.transform.position.y <= 10){
        //         rb.velocity = new Vector3(0, 10, 0);
        //     }
        //     isJumpPressed = false;
        // }
    }

    private void SpeedTimer() {
        if(speedTimeRemaining > 0){
            speedTimeRemaining -= Time.deltaTime;
        }
        
        else {
            speedTimeRemaining = speedTime;
            changeSpeed = false;
            maxSpeed = initialMaxSpeed;
            _vignette.intensity.value = 0f;
            _chromatic.intensity.value = 0f;
        }
    }

     private void ShieldTimer() {
        if(shieldTimeRemaining > 0){
            shieldTimeRemaining -= Time.deltaTime;
        }
        
        else {
            shieldTimeRemaining = shieldTime;
            shield.SetActive(false);
            protectedPlayer = false;
        }
    }
        
    // private void PlayerMovement() {

    //     /* ROTACION */
    //     // if(Input.GetKey(KeyCode.Q)) transform.Rotate(new Vector3(0,0,-0.1f));

    //     // if(Input.GetKey(KeyCode.E)) transform.Rotate(new Vector3(0,0,0.1f));

    //     /* ADELANTE ATRAS Y COSTADOS */
    //     float movX = Input.GetAxis("Horizontal");
    //     float movY = Input.GetAxis("Vertical");

    //     transform.Translate(new Vector3(movX, -movY, 0) * Time.deltaTime * speed);
    // }


    private void PlayerMovementPhy() {

        float movX = Input.GetAxis("Horizontal");
        float movY = Input.GetAxis("Vertical");
        Vector3 playerInput = new Vector3(movX,0,movY);
        rb.AddForce(playerInput * speedForce);
    }


    private void PlayerJumpPhy()
    {
        //Debug.Log(raycastDistance);
        GameObject firstWheel = transform.GetChild(7).gameObject;
        RaycastHit hit;
        if(Physics.Raycast(firstWheel.transform.position, Vector3.down, out hit, raycastDistance, groundLayer))
            isGrounded = true;
        else isGrounded = false;

        if (isGrounded) rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    }


     public void Respawn()
    {
        transform.position = initialPos;
    }

    private void OnTriggerEnter(Collider col) 
    {

        if(col.transform.gameObject.CompareTag("Bullet")){
            if(!protectedPlayer){
                onBulletHit.Invoke();
            }
            Destroy(col.transform.gameObject);
        }
        
        if(col.transform.gameObject.CompareTag("Coin")){
            onCoinHit.Invoke();
            Destroy(col.transform.gameObject);
            GameObject go = Instantiate(coinEffect, col.transform.gameObject.transform.position, transform.rotation);
            Destroy(go, 3);
        }

        if(col.transform.gameObject.CompareTag("Shield")){
            protectedPlayer = true;
            shield.SetActive(true);
            Destroy(col.transform.gameObject);
        }

        if(col.transform.gameObject.CompareTag("Finish")){
            Singleton.finishedGame = true;
            onFinish.Invoke();
        }
    }
    

    void OnTriggerStay(Collider col) {
        if(col.transform.gameObject.CompareTag("Life")){
            lifeTime += Time.deltaTime;
            if(lifeTime > 1)
            {
                onLifeHit.Invoke();
                Destroy(col.transform.gameObject);
                lifeTime = 0;
                GameObject go = Instantiate(lifeEffect, col.transform.gameObject.transform.position, transform.rotation);
                Destroy(go, 3); 
            } 
        }
    }

    private void OnTriggerExit(Collider col) {
        if(col.transform.gameObject.CompareTag("SpeedBoostPlatform")){
            changeSpeed = true;
            _vignette.intensity.value = 0.550f;
            _vignette.color.value = new Color(0.052f,1.0f,0f,1f);
            _chromatic.intensity.value = 1f;
            maxSpeed = 25f;
        }

        if(col.transform.gameObject.CompareTag("SpeedReductionPlatform")){
            changeSpeed = true; 
            _vignette.intensity.value = 0.550f;
            _vignette.color.value = new Color(0,0.6529045f,1,1);
            maxSpeed = 12.5f;
        }
    }

    private void WallHit()
    {
        onWallHit.Invoke();
    }

    private void OnCollisionEnter(Collision col) {
        if(col.transform.gameObject.CompareTag("Wall")){
            Singleton.timeRemaining = (Singleton.totalTime * 0.75f);
            Invoke("WallHit", 0.15f);
        }
    }
}

    