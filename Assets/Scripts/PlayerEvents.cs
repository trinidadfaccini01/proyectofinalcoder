using UnityEngine;
using UnityEngine.UI;

public class PlayerEvents : MonoBehaviour
{
    public void Damage(int damage)
    {
        Singleton.playerLife -= damage;
    }

    public void AddPoints(int points)
    {
        Singleton.playerPoints++;
    }

    public void AddLife(int life)
    {
        Singleton.playerLife += life;
    }

}