using UnityEngine;
using UnityEngine.SceneManagement;

public class Singleton : MonoBehaviour
{
    public static Singleton instance;

    [Header ("Audio de juego")]
    public AudioSource _audSource;

    [Header ("Player")]
    public static int playerLife;
    public static int playerPoints;

    [Header ("Estado de juego")]
    public static bool activePause = false;
    public static bool finishedGame = false;

    [ Header ("Tiempo de juego")]
    public static float totalTime = 200f;
    public static float timeRemaining = 200f;


    private void Awake() {

        if(Singleton.instance == null ){
            Singleton.instance = this;
            DontDestroyOnLoad(gameObject);
            _audSource = GetComponent<AudioSource>();
        }

        else{
            Destroy(gameObject);
        }
    }

    // private void Start()
    // {
    //     playerLife = 100;
    //     playerPoints = 0;
    // }
   
    public static void Play() {
        SceneManager.LoadScene(1);
    } 

    public void Exit() 
    {
        Application.Quit();
    }    
    public static void PlayAudio()
    {
        instance._audSource.UnPause();
    }

    public static void PauseAudio()
    {
        instance._audSource.Pause();
    }


}

