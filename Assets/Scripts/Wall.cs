using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//TIPO 1: Inicialmente esta a la derecha y se mueve hacia la izquierda (+ daño que fija)
//TIPO 2: Inicialmente esta a la izquierda y se mueve hacia la derecha (+ daño que fija)
//TIPO 3: Fija
public enum WallType {
    TYPE1,
    TYPE2,
    TYPE3
}


public class Wall : MonoBehaviour
{

    public WallType typeOfWall;

    public int damage;
    public Animator anim;

    
    // Start is called before the first frame update
    void Start()
    {
        switch(typeOfWall){
            case WallType.TYPE1:
                //Debug.Log("Tipo 1");
                break;

            case WallType.TYPE2:
                transform.parent.gameObject.transform.Rotate((new Vector3(0,180,0)));
                //Debug.Log("Tipo 2");
                break;

            case WallType.TYPE3:
                //Debug.Log("Tipo 3");
                anim.enabled = false;
                break;
            default:
                Debug.Log("Tipo de pared no asignada");
                break;
        }
        
    }

    // Update is called once per frame
    void Update(){

        
    }

}
