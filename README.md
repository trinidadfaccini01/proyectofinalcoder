_**PROYECTO FINAL - DESARROLLO DE VIDEOJUEGOS - TRINIDAD FACCINI**_
------

CAR CRASH

**Controles de juego:**
* Tecla CTRL izquierdo: cambiar de cámara
* Espacio: saltar
* Tecla P: Pausar o reaunidar sonido de ambiente del juego
* Tecla Q: Sonido de guiño
* Tecla E: Sonido de bocina
* Teclas W-S o Flecha arriba-Flecha abajo: mover el jugador hacia adelante y atrás respectivamente
* Teclas A-D o Flecha izquierda-Flecha derecha: mover el jugador hacia la derecha o a la izquierda respectivamente

En la siguiente imagen se pueden ver:
https://media.discordapp.net/attachments/1034079455016140851/1056319747169468416/image.png

------

**Juego**

El proposito de este juego es desafiar tu inteligencia y reflejos para llegar a la meta exitosamente.
Para ganar el juego deberás lograr terminarlo cumpliendo una serie de requisitos:
- Deberás hacerlo en un tiempo limite
- Deberás conseguir una cantidad determinada de puntos
- Deberás lograr llegar con vida 

A lo largo de esta extensa carretera te vas a encontrar con una serie de obstáculos que dificultaran tu paso permanentemente.
- Paredes que, al ser chocadas, dañaran tu vida (-5) y te van a enviar de nuevo al inicio del juego (no te preocupes, conservaras tus puntos!)
- Plataformas que reduciran tu velocidad (azules)
- Balas de diferentes cañones van a ser disparadas hacia vos durante todo el juego, restandote puntos de vida (-5)

No todo son obstáculos en el juego, tambien van a haber cosas que te ayudaran en tu paso por el juego
- Monedas que te otorgaran puntos. Mucha atención ya que para ganar el juego tenes que recoger minimo 15 monedas, de lo contrario no podrás ganar!
- Plataformas que aceleraran tu velocidad (verdes).
- Vidas para recuperarte del daño. Atento! Ya que deberás estar en contacto con ellas por un mínimo de dos segundos para que te puedan curar.
- Escudos para protegerte de las balas

Por último, algunas de las calles se encuentran en movimiento, no debes olvidar SALTAR ya que si te sales de la carretera se te restara vida (-10) y volveras al punto de partida.

------------

**Requisitos de la entrega**

1. Game Manager en forma se Singleton

2. Reflection probes:
    - Vidrios del auto

3. 2 casos de Raycasting:
    - Raycasting para saber si el jugador se encuentra sobre el suelo para saltar
    - Raycasting de armas de tipo 3 y 4 para dispararle al jugador

4. Fisicas
    - Jugador (auto)

5. Herencia:
    - Armas

6. TDAs:
    - Puntos de posicion de escudos 

7. Partículas (al entrar en contacto):
    - Monedas
    - Vida 

8. UI in-game funcional

9. Sistema de menues (ambos con opciones para ver los controles del juego y salir del juego):
    - Menu de inicio
    - Menu de pausa

10. Efectos de post-processing:
    - Al entrar al tunel se ve afectada la iluminación
    - Al salir del túnel se simula la encandilación
    - Al pasar por las plataformas se ve una viñeta de color azul/verde y "Chromatic aberration" para darle al jugador la sensación de ir más rápido/más lento

11. Utilización de eventos:
    - La mayoría de las acciones en el juego se llevan a cabo por eventos que se separaron por Eventos de Juego y Eventos de Jugador 

12. Luces:
    - Luces del auto
    - Luz ambiental 

13. Temporizadores:
    - Balas
    - Deshabilitacion/inhabilitacion de luz
    - Protección del Escudos
    - Duracción de reducción/aumento de velocidad

14. Inputs para movimiento de jugador
15. Uso de materiales y texturas para todos los objetos en escena
16. Sistema de camaras:
    - Una enfoca al auto desde atras a una distancia alejada
    - Otra simula que fuera la vista del conductor del auto 
17. Calculos vectoriales:
    - Para disparar las balas
18. Diferentes colisiones separadas en Layers
19. Animaciones:
    - Paredes
    - Monedas
20. Switch:
    - Para definir animacion de pared segun que tipo de pared sea


